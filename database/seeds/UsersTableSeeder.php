<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->truncate();
        DB::table('role_user')->truncate();

//        $faker = Faker\Factory::create();
        // Create Admin
        $user = User::create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'admin@domain.com',
            'password' => bcrypt('admin'),
            'role' => 'Admin'
        ]);

        // Find Admin Role
        $adminRole = Role::where('name', 'admin')->first();


        // Add User Roles
        $user->attachRole($adminRole);

        // Create Developer
        $member = User::create([
            'first_name' => 'Member',
            'last_name' => '1',
            'email' => 'member@domain.com',
            'password' => bcrypt('member'),
            'role' => 'Member',
            'is_approved' => 1

        ]);

        // Find Developer Role
        $memberRole = Role::where('name', 'member')->first();

        // Add User Roles
        $member->attachRole($memberRole);
    }
}
