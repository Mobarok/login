<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 15)->unique()->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email', 40)->unique()->nullable();
            $table->string('phone', 24)->nullable();
            $table->string('password')->nullable();
            $table->string('role', 24)->nullable();
            $table->string('verify_code')->nullable();
            $table->enum('provider', ['email', 'facebook', 'google'])->default('email');
            $table->string('social_id', 64)->nullable();
            $table->boolean('is_approved')->default(0);
            $table->boolean('is_active')->default(0);
            $table->timestamp('approved_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
