<?php

namespace App\Http\Controllers\Auth;

use App\Models\Role;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        //Check Duplicate users
        $user = $this->hasUser($data['email']);

        if($user != "Has User"){

            //  Verify Token generate by date and email MD5
            $token = md5(Carbon::today().$data['email']);
            if($this->sendVerifyToken($token, $data['email'])){

                if($user == "Not User"){
                    User::create([
                        'first_name' => $data['first_name'],
                        'last_name' => $data['last_name'],
                        'phone' => $data['phone'],
                        'email' => $data['email'],
                        'password' => Hash::make($data['password']),
                        'role' => 'member',
                        'verify_code' => $token,
                    ]);
                }elseif ($user=="Not Verify User"){

                    $user = User::where('email', $data['email'])->first();

                    $user->update([
                        'verify_code' => $token,
                        'role' => 'member',
                        'password' => bcrypt($data['password']),
                    ]);

                }

                $type  = "status";
                $message = "Please check your mail and click Confirm Verification link";

            }
        }else{
            $type  = "error";
            $message = "Already Register User";
        }

        Session::flash($type, $message);

        return true;
    }


    /**
     * @param $token
     * @param $email
     * @return bool
     */

    protected function sendVerifyToken($token, $email){

        $url = route('verify.registration', $token);

        Mail::send('common.verifyTokenMail', ['url' => $url], function ($message) use($email){

            $message->to($email);
            $message->subject('Login System Registration Confirmation');
            $message->from('info@login.com','AddAlarm');
        });

        return true;
    }

    // Email verification by users

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function verifyByUser($id){

        $user = User::where('verify_code', $id)->first();

        // Find Users Role
        $userRole = Role::where('name', $user->role)->first();

        DB::beginTransaction();
        try{

            // Add User Roles

            $user->attachRole($userRole);
            $user->update(
                [
                    'approved_date' => Carbon::today()->toDateTimeString(),
                    'is_approved' => 1,
                    'is_active' => 1
                ]
            );

            DB::commit();

        }catch (\Exception $e){
            DB::rollback();
        }


        return redirect('/');
    }


    // User Available Check

    /**
     * @param $email
     * @return string
     */

    private function hasUser($email){

        $verifyUser = User::where('email', $email)->where('is_approved',1)->first();

        if($verifyUser)
        {
            return "Has User";

        } else{
            $notVerifyUser = User::where('email', $email)
                ->whereNull('is_approved')
                ->first();
            if($notVerifyUser){
                return "Not Verify User";
            }else{
                return "Not User";
            }
        }

    }
}
