<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**

     * Handle Social login request

     *

     * @return response

     */

    public function socialLogin($social)

    {

        return Socialite::driver($social)->redirect();

    }

    /**

     * Obtain the user information from Social Logged in.

     * @param $social

     * @return Response

     */

    public function handleProviderCallback($social)

    {

        $userSocial = Socialite::driver($social)->user();

        $user = User::where(['email' => $userSocial->getEmail()])->first();

        if($user){

            Auth::login($user);

            return redirect()->action('Member\DashboardController@index');

        }else{

            $user = $this->createUser($userSocial, $social);

            return redirect()->action('Member\DashboardController@index');
        }

    }


    public function createUser($user, $social){

        $data = [];

        $data['name'] = $user->name;
        $data['email'] = $user->email;
        $data['is_approved'] = 1;
        $data['is_active'] = 1;
        $data['provider'] = $social;
        $data['social_id'] = $user->id;
        $data['role'] = 'Member';

        $member  = User::create($data);;
        // Find Developer Role
        $memberRole = Role::where('name', 'member')->first();

        // Add User Roles
        $member->attachRole($memberRole);

        return $member;
    }


    /**
     * Admin login
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // Blacklisted roles that are not able to login in admin area
        $blacklistedRoles = ['member'];

        // Find Admin
        $user = User::where('email', $request->input('email'))->first();

        if (!$user) return $this->sendFailedLoginResponse($request);

        if (!$user->hasRole($blacklistedRoles)) {
            if (Auth::attempt(['email' => $user->email, 'password' => $request->input('password')])) {
                return redirect($this->redirectTo);
            }
        } else {
            if (Auth::attempt(['email' => $user->email, 'password' => $request->input('password'), 'is_approved' => 1])) {
                if($user->hasRole('member'))
                    return redirect()->route('member.dashboard');
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
