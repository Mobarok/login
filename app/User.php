<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'password',
        'provider', 'social_id', 'is_approved', 'is_active', 'approved_date',
        'created_by', 'updated_by', 'role', 'verify_code'
    ];

    /**
     * @var array
     */
    protected static $logAttributes = [
        'first_name', 'last_name', 'email', 'phone', 'password',
        'provider', 'social_id', 'is_approved', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'provider', 'social_id', 'is_active', 'approved_date', 'sms_token', 'fcm_token', 'photo',
        'created_by', 'updated_by'
    ];
}
