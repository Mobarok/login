<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Login System') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Styles -->

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">



    <!-- Scripts -->
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
</head>
<body>
    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><img src="{{asset("img/fl-logo.svg") }}" width="200px" height="65px"/></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Page 1-1</a></li>
                            <li><a href="#">Page 1-2</a></li>
                            <li><a href="#">Page 1-3</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Page 2</a></li>
                </ul>
                <div>
                    <div class="LoginSection pull-right">
                        <a class="signin" href="#" data-toggle="modal" data-target="#login" >Sign In</a>
                        <a class="signup" href="#" data-toggle="modal" data-target="#signup" >Create Free Account</a>
                        {{--<a id="header-post-project" href="#" class="btn btn-small btn-post-project">--}}
                            {{--Post a Project--}}
                        {{--</a>--}}
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <div class="body">


        <div class="modal fade" id="login">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- header -->
                <div class="modal-header">
                    <div id="page" class="bootstrap">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2>Sign In</h2>
                        <div id="register-link">
                            <small>Not a member yet?&nbsp; <a href="javascript:;">Register now.</a></small>
                        </div>
                        <div class="form-group">
                            <a class="btn btn-block btn-lg btn-facebook " href="{{ url('auth/facebook') }}">
                                <i class="fa fa-facebook"></i> Sign In
                            </a>
                            <a class="btn btn-block btn-lg btn-google " href="{{ url('auth/google') }}">
                              <i class="fa fa-google-plus"></i>  Sign In
                            </a>
                        </div>
                        <div class="form-group">
                            <span id="fb-error-msg" style="color:red"></span>
                            <div id="divider-container">
                                <div id="divider-or">or</div>
                                <hr>
                            </div>
                        </div>

                        <form method="POST" action="{{ route('login') }}" autocomplete="off">

                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="email" class=" col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                             <div class="form-group">
                                <div class="span6 pull-right">
                                    <small><a href="/" style="margin-top: 8px;" id="forgot-password-link">Forgot your password?</a></small>
                                </div>
                                <div class="span6 pull-left">
                                    <label for="securityPersist" class="checkbox">
                                        <label class="switch">
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}>
                                            <span class="slider round"></span>
                                        </label>


                                        <small class="remember">Remember me</small>
                                    </label>

                                </div>
                            </div>
                            <div class="form-group submit-box">
                                <input id="submit-button" type="submit" class="btn btn-signin btn-block btn-lg" value="SIGN IN">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>


        <div class="modal fade" id="signup">
            <div class="modal-dialog">

                <div class="modal-content">

                    <div class="modal-header">
                    <div class="col-md-6">

                        <div class="text-center signup-tag">
                            <img src="{{asset('img/fl-logo.svg')}}" width="70%"/>
                            <hr>
                           <h4 class="signupH3">Everything you need to buy or sell a home – under one roof
                            </h4><br>
                        </div>
                        <div class="padding-top-10">
                            <div class="col-sm-2 ">
                                <i class="fa fa-search"></i>
                            </div>
                            <div class="col-sm-10 margin-top-10">
                                A better way to buy or sell
                                <br>
                                We’ve made it simple to manage your entire transaction all in one place and provide you with personal service from start to finish.
                                Homes only on FreelanceWork
                            </div>
                        </div>
                        <div class="padding-top-10">
                            <div class="col-sm-2 margin-top-10">
                                <i class="fa fa-search"></i>
                            </div>
                            <div class="col-sm-10">
                                A better way to buy or sell
                                <br>
                                We’ve made it simple to manage your entire transaction all in one place and provide you with personal service from start to finish.
                                Homes only on FreelanceWork
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <!-- header -->
                            <div id="page" class="bootstrap">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h2>Create Account</h2>
                                <div id="register-link">
                                    <small>Already have an account? <a href="javascript:;">Sign In.</a></small>
                                </div>
                                <div class="form-group">
                                    <a class="btn btn-block btn-lg btn-facebook " href="{{ url('auth/facebook') }}">
                                        <i class="fa fa-facebook"></i> Sign In
                                    </a>
                                    <a class="btn btn-block btn-lg btn-google " href="{{ url('auth/google') }}">
                                        <i class="fa fa-google-plus"></i>  Sign In
                                    </a>
                                </div>
                                <div class="form-group">
                                    <span id="fb-error-msg" style="color:red"></span>
                                    <div id="divider-container">
                                        <div id="divider-or">or</div>
                                        <hr>
                                    </div>
                                </div>

                                <form method="POST" action="{{ route('register') }}" autocomplete="off">

                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="first_name" class=" col-form-label text-md-right">{{ __('First Name') }}</label>
                                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name" class=" col-form-label text-md-right">{{ __('Last Name') }}</label>
                                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="phone" class=" col-form-label text-md-right">{{ __('MOBILE (OPTIONAL)') }}</label>
                                        <input id="phone" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" >

                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class=" col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif

                                    </div>
                                    <div class="form-group">
                                        Are you interested in Auction Properties?<br/>
                                        <input type="checkbox" /> Yes
                                    </div>


                                    <div class="form-group submit-box">
                                        <input id="submit-button" type="submit" class="btn btn-signin btn-block btn-lg" value="Create Account">
                                    </div>

                                    <div class="form-group">
                                        <small>I accept the
                                            <a href="/" style="margin-top: 8px;" id="forgot-password-link">Terms of Use</a>,
                                            <a href="/" style="margin-top: 8px;" id="forgot-password-link">Privacy Policy </a> and
                                            <a href="/" style="margin-top: 8px;" id="forgot-password-link">
                                                Auction Participation Agreement</a>
                                        </small>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
