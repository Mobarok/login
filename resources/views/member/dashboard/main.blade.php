<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/2/2017
 * Time: 10:42 PM
 */

$data['breadcrumb'] = [
[
'name' => 'Home',
'href' => route('member.dashboard'),
'icon' => 'fa fa-home',
],
[
'name' => 'Dashboard',
]
];

$data['data'] = [
'name' => 'Dashboard',
'title'=>' Member Dashboard ',
'heading' => 'Dashboard',
];

?>

@extends('member.layout.master', $data)

@section('contents')

    <h1> Hello, Member </h1>

@endsection
